@extends ('layouts.master')

@section ('content')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

	<h1>Create a post</h1>

		<form method="POST" action="/posts">
			{{ csrf_field() }}


	  <div class="form-group">
	    <label for="title">Title:</label>
	    <input type="text" class="form-control" id="title" name="title" >
	  </div>
	  <div class="form-group">
	    <label for="body">Body</label>
	    <textarea id="body" name="body" class="form-control" ></textarea>
	  </div>
	
	  <button type="submit" class="btn btn-primary">Publish</button>
	</form>


@include ('layouts.errors')

@endsection