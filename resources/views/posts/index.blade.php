@extends('layouts.master')


@section ('content')
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

      

    <main role="main" class="container">
      <div class="row">
        <div class="col-md-8 blog-main">

          @foreach ($posts as $post)

            @include('posts.post')

          @endforeach
         
          <nav class="blog-pagination">
            <a class="btn btn-outline-primary" href="#">Older</a>
            <a class="btn btn-outline-secondary disabled" href="#">Newer</a>
          </nav>

        </div><!-- /.blog-main -->

        
      </div><!-- /.row -->

    </main><!-- /.container -->

@endsection
