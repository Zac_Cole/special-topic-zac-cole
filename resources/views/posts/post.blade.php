 <h3 class="pb-3 mb-4 font-italic border-bottom">
         
          </h3>

          <div class="blog-post">
            <h2 class="blog-post-title">

                <a href="/posts/{{$post->id}}">
                  {{$post->title}}
                </a>
            </h2>

            <p class="blog-post-meta">

              {{$post->user->name}} on
              {{$post->created_at->toFormattedDateString()}}</p>

            {{$post->body}}
          </div>

