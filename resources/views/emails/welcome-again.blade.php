@component('mail::message')
# Introduction

Thank you for registering

@component('mail::button', ['url' => 'https://laracasts.com'])
Start Browsing
@endcomponent

@component('mail::panel', ['url' => ''])
Insperational quote to go here
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
