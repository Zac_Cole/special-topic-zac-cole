<?php

namespace Tests\Feature;

use App\Post;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{

    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        //Given i have 2 records in the DB that are posts, and each is posted 1 month apart


        $first = factory(Post::class)->create();
        $second = factory(Post::class)->create([

            'created_at' => \Carbon\Carbon::now()->subMonth()
        ]);



        //when i fetch the archives


        $posts = Post::archives();

        

        //responce should be in the proper format

        $this->assertEquals([

            [
                "year" => $first->created_at->format('Y'),
                "month" => $first->created_at->format('F'),
                "published" => 1


            ],

            [
                "year" => $second->created_at->format('Y'),
                "month" => $second->created_at->format('F'),
                "published" => 1


            ],

        ], $posts);


    }
}
