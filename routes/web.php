<?php







Route::post('/posts/{post}/comments', 'CommentsController@store');
Route::get('/','PostsController@index')->name('home');
Route::get('/posts/create','PostsController@create');
Route::post('/posts', 'PostsController@store');

Route::get('/posts/tags/{tag}', 'TagsController@index');



Route::get('/posts/{post}','PostsController@show');

// Route::get('/tasks', 'TasksController@index');
// Route::get('/tasks/{task}', 'TasksController@show');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/register', 'RegistrationController@create');
Route::post('/register', 'RegistrationController@store');

Route::post('/login', 'SessionsController@store');
Route::get('/login', 'SessionsController@create');

Route::get('/logout', 'SessionsController@destroy');