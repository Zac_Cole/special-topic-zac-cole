<?php

namespace App\Http\Controllers;


use App\Http\Requests\RegistationForm; 


class RegistrationController extends Controller
{
    public function create()
    {
    	return view ('registration.create');
    }

      public function store(RegistrationForm $form)
    {
    	
        $form->persist();
    	
        session()->flash('message', 'Thanks for signing up');

    	// Redirect to the home page

    	return redirect()->home();

    }
}
